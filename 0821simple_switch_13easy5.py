# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# ryu-manager --verbose --observe-links ofctl_rest.py simple_switch_13easy5.py rest_topology.py
from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER,  DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import ether_types
from ryu.app import simple_monitor_13
import urllib2
import array
from operator import itemgetter, attrgetter 
import os
import re,time
import pycurl
from ryu.lib import hub
from shutil import copyfile



class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
	print '__init__##############################'
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.datapaths = {}
	self.switches = {}
        self.mac_to_port = {}
        self.ipv4_to_port ={}
        #self.monitor_thread = hub.spawn(self._monitor)
	#time.sleep(15)
        #self.gw1 = ('10.10.1.98','60:e3:27:10:26:89','106528729212553')
        #self.gw2 = ('10.10.1.99','60:e3:27:08:11:b4','106528728682932')
	#self.dp_to_mac = [('106528728707019','60:e3:27:08:6f:cb'),('106528728683083','60:e3:27:08:12:4b'),('106528729212553','60:e3:27:10:26:89'),('106528728682932','60:e3:27:08:11:b4'),('106528729537196','60:e3:27:15:1a:ac')]



    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if not datapath.id in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def _monitor(self):
        while True:
            for dp in self.datapaths.values():
                self._request_stats(dp)
                hub.sleep(120)
            hub.sleep(120)
    
    def _request_stats(self, datapath):
        self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
	print '_request_stats##############################'
        #req = parser.OFPFlowStatsRequest(datapath)
        #datapath.send_msg(req)

        req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_ANY)
        datapath.send_msg(req)

    
    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_reply_handler(self, ev):
        body = ev.msg.body

        self.logger.info('datapath         port     '
                         'rx-pkts  rx-bytes rx-error '
                         'tx-pkts  tx-bytes tx-error')
        self.logger.info('---------------- -------- '
                         '-------- -------- -------- '
                         '-------- -------- --------')
        gw1 = ('10.10.1.98','84:16:f9:1e:d7:eb','145234203695083')
        gw2 = ('10.10.1.99','60:e3:27:08:11:b4','106528728682932')
        for stat in sorted(body, key=attrgetter('port_no')):
            
            self.logger.info('%016x %8x %8d %8d %8d %8d %8d %8d', 
                             ev.msg.datapath.id, stat.port_no,
                             stat.rx_packets, stat.rx_bytes, stat.rx_errors,
                             stat.tx_packets, stat.tx_bytes, stat.tx_errors)
            print 'ID',ev.msg.datapath.id,gw1[2],gw2[2]
            print 'NO',stat.port_no
            print 'COUNT',stat.rx_packets
            time.sleep(1)
            if ev.msg.datapath.id == eval(gw1[2]) and stat.port_no == 1 :
                f = open('count'+str(gw1[2])+'.txt', 'w')
                f.write(str(stat.rx_packets))
                f.close() 
                print 'gw1count',stat.rx_packets
                time.sleep(1)
                
            elif ev.msg.datapath.id == eval(gw2[2]) and stat.port_no == 1:
                f = open('count'+str(gw2[2])+'.txt', 'w')
                f.write(str(stat.rx_packets))
                f.close() 
                print 'gw2count',stat.rx_packets
                time.sleep(1)
    def channelselect(self):
	##,('10.10.1.15','10.10.3.15','')
	print 'host find##################'
	hostdpid = [('10.10.1.25','00008416f91a3903','145234203392259'),('10.10.1.15','000018d6c70d13ce','27310741590990')]
	for i in range(len(hostdpid)):
		datapath = self.switches.get(int(hostdpid[i][2]))
		ofp = datapath.ofproto
        	ofp_parser = datapath.ofproto_parser
		parser = datapath.ofproto_parser
		instructions = []
		cookie = cookie_mask = 0
        	idle_timeout = hard_timeout = 0
        	buffer_id = ofp.OFP_NO_BUFFER
		match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str('10.10.2.0/24'))
		init = parser.OFPFlowMod(datapath, 0, 0, 2, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0, match, instructions)
		datapath.send_msg(init)
		crl_ip = 'http://10.10.1.168:8080/v1.0/topology/hosts/'+str(hostdpid[i][1]) #controller WLAN IP
		data = urllib2.urlopen(crl_ip)
		hostip = []
		a = data.read()
		v = eval(a) 
		for b in v:   
			print b['ipv4'] , 'v'
			for a in b['ipv4']:
				if a.startswith('10.10.2'):
					hostip.append(a)
					print a
		print 'channel ip' ,hostip 
		for a in range(1,len(hostip),2):
			table_id = 2
			priority = 12300
			match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(hostip[a]))
			actions = [ofp_parser.OFPActionOutput(3)]
			inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
			req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
			datapath.send_msg(req)
    def onehopchange(self,a,b):
	with open('pathcost.txt') as file:
		array0 = [[str(digit) for digit in line.split()] for line in file]
	f = open('pathcost.txt', 'w')
	#print array0
	for j in range(len(array0)):
	    if array0[j][0] == a and array0[j][1] == b:
		array0[j][5] = float(array0[j][5])+float(5.077)
		print array0[j][5] ,'1'
	    elif array0[j][0] == b:
		array0[j][5] = float(array0[j][5])+float(3.794)
		print array0[j][5], '2'
	    elif array0[j][1] == b:
		array0[j][5] = float(array0[j][5])+float(3.794)
		print array0[j][5], '3'
	    f.writelines(array0[j][0] + '\t' + array0[j][1] + '\t' + array0[j][2] + '\t' + array0[j][3] + '\t' + array0[j][4] + '\t' + str(array0[j][5]) + '\t' + '\n')
    def multihopchange(self,a,b):
	with open('pathcost.txt') as file:
		array0 = [[str(digit) for digit in line.split()] for line in file]
	f = open('pathcost.txt', 'w')
	#print array0
	for j in range(len(array0)):
            for i in range(len(a)-1):
                if array0[j][0] == a[len(a)-1] and array0[j][1] == b:
                    array0[j][5] =  float(array0[j][5])+float(5.077)
                    print array0[j][5] ,'1'
                    break
                elif array0[j][0] == a[i] and array0[j][1] == a[i+1]:
                    array0[j][5] = float(array0[j][5]) + float(5.077)
                    print array0[j][5], '2'
                    break
            else:
                for i in range(len(a) - 1):
                    if array0[j][0] == b or array0[j][0] == a[i+1]:
                        array0[j][5] =  float(array0[j][5])+float(3.794)
                        print array0[j][5], '3'
                        break
                    elif array0[j][1] == b or array0[j][1] == a[i+1]:
                        array0[j][5] = float(array0[j][5]) + float(3.794)
                        print array0[j][5], '4'
                        break
            f.writelines(array0[j][0] + '\t' + array0[j][1] + '\t' + array0[j][2] + '\t' + array0[j][3] + '\t' + array0[j][4] + '\t' + str(array0[j][5]) + '\t' + '\n')
    def onehopchange2(self,a,b):
	with open('pathcost2.txt') as file:
		array0 = [[str(digit) for digit in line.split()] for line in file]
	f = open('pathcost2.txt', 'w')
	#print array0
	for j in range(len(array0)):
	    if array0[j][0] == a and array0[j][1] == b:
		array0[j][5] = float(array0[j][5])+float(5.077)
		print array0[j][5] ,'1'
	    elif array0[j][0] == b:
		array0[j][5] = float(array0[j][5])+float(3.794)
		print array0[j][5], '2'
	    elif array0[j][1] == b:
		array0[j][5] = float(array0[j][5])+float(3.794)
		print array0[j][5], '3'
	    f.writelines(array0[j][0] + '\t' + array0[j][1] + '\t' + array0[j][2] + '\t' + array0[j][3] + '\t' + array0[j][4] + '\t' + str(array0[j][5]) + '\t' + '\n')
    def multihopchange2(self,a,b):
	with open('pathcost2.txt') as file:
		array0 = [[str(digit) for digit in line.split()] for line in file]
	f = open('pathcost2.txt', 'w')
	#print array0
	for j in range(len(array0)):
            for i in range(len(a)-1):
                if array0[j][0] == a[len(a)-1] and array0[j][1] == b:
                    array0[j][5] =  float(array0[j][5])+float(5.077)
                    print array0[j][5] ,'1'
                    break
                elif array0[j][0] == a[i] and array0[j][1] == a[i+1]:
                    array0[j][5] = float(array0[j][5]) + float(5.077)
                    print array0[j][5], '2'
                    break
            else:
                for i in range(len(a) - 1):
                    if array0[j][0] == b or array0[j][0] == a[i+1]:
                        array0[j][5] =  float(array0[j][5])+float(3.794)
                        print array0[j][5], '3'
                        break
                    elif array0[j][1] == b or array0[j][1] == a[i+1]:
                        array0[j][5] = float(array0[j][5]) + float(3.794)
                        print array0[j][5], '4'
                        break
            f.writelines(array0[j][0] + '\t' + array0[j][1] + '\t' + array0[j][2] + '\t' + array0[j][3] + '\t' + array0[j][4] + '\t' + str(array0[j][5]) + '\t' + '\n')
    def check(self):
    	with open('cost.txt') as file,open('oldcost.txt') as f1:
        	array0 = [[str(digit) for digit in line.split()] for line in file]
        	array1 = [[str(digit) for digit in line.split()] for line in f1]
        #print 'array0' ,array0
        #print 'array1', array1
        global checknode
        if len(array1) != len(array0):
            print 'channel A node not same'
            checknode=1
            return checknode

        for j in range(len(array1)):
            #print array1[j][0] ,array1[j][1]
            for i in range(len(array0)):
                if array1[j][0] == array0[i][0] and array1[j][1] == array0[i][1]:
                    if abs(float(array1[j][5])-float(array0[i][5]))<400:
                        #print 'good'
                        checknode=0
                    else :
                        #print 'bad1'
                        checknode=1
                        return checknode
	with open('cost2.txt') as file,open('oldcost2.txt') as f1:
        	array0 = [[str(digit) for digit in line.split()] for line in file]
        	array1 = [[str(digit) for digit in line.split()] for line in f1]
        #print 'array0' ,array0
        #print 'array1', array1
        global checknode
        if len(array1) != len(array0):
            print 'channel B node not same'
            checknode=1
            return checknode

        for j in range(len(array1)):
            #print array1[j][0] ,array1[j][1]
            for i in range(len(array0)):
                if array1[j][0] == array0[i][0] and array1[j][1] == array0[i][1]:
                    if abs(float(array1[j][5])-float(array0[i][5]))<400:
                        #print 'good'
                        checknode=0
                    else :
                        #print 'bad1'
                        checknode=1
                        return checknode
        checknode=0
        return checknode
        
    def ryu_path(self,ip):
        ##to get controller OLSR topology ##
        ##and to get the shortest path to GW ##

        with open('pathcost.txt') as file:
             array0 = [[str(digit) for digit in line.split()] for line in file] 
        				
        array1 = []
        array = []
        for ele in array0:                    
          if ele[0] != '10.10.1.168':
            #print 'A',ele[0]
            array1.append(ele)
        for ele in array1:
          if ele[1] != '10.10.1.168':
            array.append(ele)
        #print array
        global b,l
        b = sorted(array, key=itemgetter(0)) 

        l = []
        k = ''
        f = open('B'+str(ip)+'.txt', 'w')
        for j in range(len(b)):             
              if b[j][0] > k:
                   k = b[j][0]
                   l.append(b[j][0])
        aps = l 							
        for i in range(len(l)):				
              globals()['X%s' % ( i)]= ''       
              globals()['a%s' % (i)]= ''
              ('X'+str(i)+'='+ str(globals()['X%s' % (i)]))
              ('a'+str(i)+'='+ str(globals()['a%s' % (i)]))
              for j in range(len(b)):
                   w = b[j][0],b[j][1]+' '+b[j][5]
                   #print w
                   if w[0] == l[i] :
                        globals()['a%s' % (i)] = globals()['a%s' % (i)]+','+w[1]
                        globals()['X%s' % (i)] = w[0]
              f.writelines(globals()['X%s' % (i)]+globals()['a%s' % (i)]+'\n')
        f.close()

        graph = {}
        temp = ''
        with open('B'+str(ip)+'.txt') as f:
            for line in f:
               info = line.split(',')
               key = info[0]
               key = key.strip()
               graph[key] = []
               if key != str(ip):
                  for ele in info[1:]:
                     node,cost = ele.split()        
                     node = node.strip()
                     cost = cost.strip()
                     cost  = float(cost)
                     temp = node,cost
                     graph[key].append(temp)
        
        J = {}
        bigNum = 1e4
        for key in graph:
             J[key] = bigNum
        J[str(ip)] = 0.0 
        iterTimes = 0
        while True:
             newJ = {}
             for key in graph:
                  if key == str(ip):
                        newJ[str(ip)] = 0.0
                  else:
                       nodes = graph[key]
                       newJ[key] = min(node[1]+J[node[0]] for node in nodes)
             iterTimes += 1
             if newJ == J:
                  break
             else:
                 J = newJ

        #print J
        global A,newJ,aps
        A = {}
        for i in range(len(aps[0:-1])):
            globals()['path%s' % (i)]= []
            ('path'+str(i)+'='+ str(globals()['path%s' % (i)]))
        for i in range(len(aps[0:-1])):
             start = aps[i]
             sum_cost = 0.0
             while start != str(ip):
               #print(start)
               globals()['path%s' % (i)].append(start)
               running_min = 1e4
               for dest,cost in graph[start]:
                 cost_of_path = cost + J[dest]
                 if cost_of_path<running_min:
                   running_min = cost_of_path
                   min_cost = cost
                   min_dest = dest
                 start = min_dest
                 sum_cost = min_cost
                 A[i] = str(globals()['path%s' % (i)])
        
        return A,newJ,aps


    def ryu_path2(self,ip):
        ##to get controller OLSR topology ##
        ##and to get the shortest path to GW ##

        with open('pathcost2.txt') as file:
             array0 = [[str(digit) for digit in line.split()] for line in file] 
        				
        array1 = []
        array = []
        for ele in array0:                    
          if ele[0] != '10.10.1.168':
            #print 'A',ele[0]
            array1.append(ele)
        for ele in array1:
          if ele[1] != '10.10.1.168':
            array.append(ele)
        #print array
        global b,l
        b = sorted(array, key=itemgetter(0)) 

        l = []
        k = ''
        f = open('B'+str(ip)+'.txt', 'w')
        for j in range(len(b)):             
              if b[j][0] > k:
                   k = b[j][0]
                   l.append(b[j][0])
        aps = l 							
        for i in range(len(l)):				
              globals()['X%s' % ( i)]= ''       
              globals()['a%s' % (i)]= ''
              ('X'+str(i)+'='+ str(globals()['X%s' % (i)]))
              ('a'+str(i)+'='+ str(globals()['a%s' % (i)]))
              for j in range(len(b)):
                   w = b[j][0],b[j][1]+' '+b[j][5]
                   #print w
                   if w[0] == l[i] :
                        globals()['a%s' % (i)] = globals()['a%s' % (i)]+','+w[1]
                        globals()['X%s' % (i)] = w[0]
              f.writelines(globals()['X%s' % (i)]+globals()['a%s' % (i)]+'\n')
        f.close()

        graph = {}
        temp = ''
        with open('B'+str(ip)+'.txt') as f:
            for line in f:
               info = line.split(',')
               key = info[0]
               key = key.strip()
               graph[key] = []
               if key != str(ip):
                  for ele in info[1:]:
                     node,cost = ele.split()        
                     node = node.strip()
                     cost = cost.strip()
                     cost  = float(cost)
                     temp = node,cost
                     graph[key].append(temp)
        
        J = {}
        bigNum = 1e4
        for key in graph:
             J[key] = bigNum
        J[str(ip)] = 0.0 
        iterTimes = 0
        while True:
             newJ = {}
             for key in graph:
                  if key == str(ip):
                        newJ[str(ip)] = 0.0
                  else:
                       nodes = graph[key]
                       newJ[key] = min(node[1]+J[node[0]] for node in nodes)
             iterTimes += 1
             if newJ == J:
                  break
             else:
                 J = newJ

        #print J
        global A,newJ,aps
        A = {}
        for i in range(len(aps[0:-1])):
            globals()['path%s' % (i)]= []
            ('path'+str(i)+'='+ str(globals()['path%s' % (i)]))
        for i in range(len(aps[0:-1])):
             start = aps[i]
             sum_cost = 0.0
             while start != str(ip):
               #print(start)
               globals()['path%s' % (i)].append(start)
               running_min = 1e4
               for dest,cost in graph[start]:
                 cost_of_path = cost + J[dest]
                 if cost_of_path<running_min:
                   running_min = cost_of_path
                   min_cost = cost
                   min_dest = dest
                 start = min_dest
                 sum_cost = min_cost
                 A[i] = str(globals()['path%s' % (i)])
        
        return A,newJ,aps


    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
	
        datapath = ev.msg.datapath
	self.switches[datapath.id] = datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        msg = ev.msg
        ## To get RYU switch numbers###
        crl_ip = 'http://10.10.1.168:8080/stats/switches' #controller WLAN IP
        data = urllib2.urlopen(crl_ip)
        a = data.read()
        ovs = eval(a)
	print ovs, 'OVS'
        global ovs,ovsno
        ovsno = len(ovs)
        print ovsno,'Connecting OVS Number'
	
        
	time.sleep(5)
	
                       
        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()

        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
    
        self.add_flow(datapath, 10, match, actions)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
	print 'add_flow#######################################'
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

        if buffer_id:
	    #print 'buffer_id#######################################'
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority,match=match,
                                    instructions=inst)
            
        else:
	    #print 'NO  buffer_id#######################################'
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
	print 'packet_in#################################'
	
			
	global checknode
	self.check()
	if checknode == 0 :
	    print'node not change'
	    return
	copyfile('cost.txt', 'oldcost.txt')
	copyfile('cost.txt', 'pathcost.txt')
	copyfile('cost2.txt', 'oldcost2.txt')
	copyfile('cost2.txt', 'pathcost2.txt')
        global ovs,ovsno
        #time.sleep(1)
	alldpid = ('145234203695083','145234203392259','27310741594218','27310741590990')
	####channelA int
        gw1 = ('10.10.1.98','18:d6:c7:0d:56:a5','27310741608024')
        gw2 = ('10.10.1.99','84:16:f9:1e:85:51','145234203673937')
	gw1ch2 = ('10.10.3.98','84:16:f9:1a:17:f4','145234203695083')
        gw2ch2 = ('10.10.3.99','84:16:f9:1e:58:42','145234203662402')
        ctrlIP = '10.10.1.168'
        mapipch1 = ('10.10.1.25','10.10.1.15')
	mapipch2 = ('10.10.3.25','10.10.3.15')
        dp_to_mac = [('10.10.1.25','145234203392259','84:16:f9:1a:39:03'),('10.10.1.45','27310741594218','18:d6:c7:0d:20:6a'),('10.10.1.15','27310741590990','18:d6:c7:0d:13:ce'),('10.10.1.55','145234203657311','84:16:f9:1e:44:5f'),('10.10.3.55','27310741570752','18:d6:c7:0c:c4:c0'),('10.10.3.25','145234203662428','84:16:f9:1e:58:5c'),('10.10.3.15','145234203695054','84:16:f9:1e:d7:ce'),('10.10.3.45','145234203391803','c2:a4:d6:f4:24:42')]
	
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        #### ('10.10.3.55','27310741570752','18:d6:c7:0c:c4:c0')('10.10.3.25','145234203662428','84:16:f9:1e:58:5c')('10.10.3.99','145234203662402','84:16:f9:1e:58:42')('10.10.3.98','145234203695083','84:16:f9:1e:d7:eb')('10.10.3.15','145234203695054','84:16:f9:1e:d7:ce')('10.10.3.45','145234203391803','c2:a4:d6:f4:24:42')
        ## use re to get the which switches that connect to RYU controller##
        #m = '..:..:..:..:..:..'
        #crl_ip = 'http://10.10.1.168:8080/v1.0/topology/switches' #controller WLAN IP
        #data = urllib2.urlopen(crl_ip)
        #a = data.read()
        #v = eval(a)    
        #print v
        #ryu_sw = re.findall(m,a)
        #print ryu_sw
        #time.sleep(2)
        ## to get controller arp table##
        e = os.popen("arp -i wlp2s0").read()
        e = e[81:-1]
        f = open('arp.txt', 'w')
        f.write(str(e))
        f.close() 
        with open('arp.txt') as file:
              f = [[str(digit) for digit in line.split()] for line in file]
        arp_table= []
        for l in range(len(f)):
             x = f[l][0],f[l][2]
             arp_table.append(x)
        print 'arp_table',arp_table
        
        ## To get RYU switch numbers###
        crl_ip = 'http://10.10.1.168:8080/stats/switches' #controller WLAN IP
        data = urllib2.urlopen(crl_ip)
	print 'data' ,data
        a = data.read()
        ovs1 = eval(a)
        ovsnow = len(ovs1)
	print 'ovs1' ,ovs1
	print 'ovsnow' ,ovsnow

        msg = ev.msg
        datapath = msg.datapath
	#print 'datapath' ,datapath
        
        ofproto = datapath.ofproto
	#print 'ofproto' ,ofproto
        parser = datapath.ofproto_parser
	#print 'parser' ,parser
        in_port = msg.match['in_port']
	#print 'in_port' ,in_port

        pkt = packet.Packet(data=msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
          
        dst = eth.dst
        src = eth.src
        dpid = datapath.id
        #print 'dpid',dpid
        ## dpid to MAC addr.####
        #for a in range(len(dp_to_mac)):
        #   print dp_to_mac[a][1],dpid
        #   if dp_to_mac[a][1] == str(dpid):
        #        src1 = ''
        #        src1 = dp_to_mac[a][2]
        #        print 'src1' ,src1
				  
        ofp = datapath.ofproto
        ofp_parser = datapath.ofproto_parser
        instructions = []
        cookie = cookie_mask = 0
        idle_timeout = hard_timeout = 0
        buffer_id = ofp.OFP_NO_BUFFER
        ######channelselect#############################
	#self.channelselect()
	#print 'channelselect end##########################'
	#return
	#init,init1 Delete all table 30,100
	##channel All del table 30 100 flow
	#for i in range(len(dp_to_mac)):
	#	datapath = self.switches.get(int(dp_to_mac[i][1]))
        #	init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0, None, instructions)
        #	init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0, None, instructions)
        #	datapath.send_msg(init)
        #	datapath.send_msg(init1)
	#####all node sort#######################
	print '###########################node sort start#############'
	
	global Agw1,newJgw1
	self.ryu_path(gw1[0])
	newJgw1 = newJ
	print 'newjgw1' ,newJgw1
	Agw1 = A
	print 'AGW1' ,Agw1
	global Agw2,newJgw2
	self.ryu_path(gw2[0])
	newJgw2 = newJ
	print 'newJgw2' ,newJgw2
	Agw2 = A
	print 'AGW2' ,Agw2

	global Agw1ch2,newJgw1ch2
	self.ryu_path2(gw1ch2[0])
	newJgw1ch2 = newJ
	print 'newjgw1' ,newJgw1ch2
	Agw1ch2 = A
	print 'AGW1' ,Agw1
	global Agw2,newJgw2ch2
	self.ryu_path2(gw2ch2[0])
	newJgw2ch2 = newJ
	print 'newJgw2' ,newJgw2ch2
	Agw2ch2 = A
	print 'AGW2' ,Agw2ch2

	list = []
	for a in range(len(mapipch1)):
		if newJgw1[mapipch1[a]] < newJgw2[mapipch1[a]]:
			list.append([mapipch1[a],newJgw1[mapipch1[a]]])
		elif newJgw1[mapipch1[a]] > newJgw2[mapipch1[a]]:
			list.append([mapipch1[a],newJgw2[mapipch1[a]]])
	for a in range(len(mapipch2)):
		if newJgw1ch2[mapipch2[a]] < newJgw2ch2[mapipch2[a]]:
			list.append([mapipch2[a],newJgw1ch2[mapipch2[a]]])
		elif newJgw1ch2[mapipch2[a]] > newJgw2ch2[mapipch2[a]]:
			list.append([mapipch2[a],newJgw2ch2[mapipch2[a]]])

	natnode = sorted(list,key=lambda x : x[1])
	print 'natnode',natnode
	print natnode[2][0]
	print '#####################################sort end##############################'
	#return
        # Add shortest path to ovs swithes and the backup path

        #################
        ## NORMAL RULE ##
        #################
       
		##Get Ryu_path GW1###
		#################
	#global Agw1,newJgw1
	#self.ryu_path(gw1[0])
	#newJgw1 = newJ
	#print 'newjgw1' ,newJgw1
	#Agw1 = A
	#print 'AGW1' ,Agw1
		##Get Ryu_path GW2###
		#####################   
	#global Agw2,newJgw2
	#self.ryu_path(gw2[0])
	#newJgw2 = newJ
	#print 'newJgw2' ,newJgw2
	#Agw2 = A
	#print 'AGW2' ,Agw2
		###Choose Better GW###
		##### Compare GW1 & GW2 to define path ##################
	#datapath.send_msg(init)
	#datapath.send_msg(init1)
	#with open('count'+str(gw1[2])+'.txt') as file:
	   #gw1count= file.read()
	#print 'GW1 Counter',gw1count

	#with open('count'+str(gw2[2])+'.txt') as file:
	   #gw2count= file.read()
	#print 'GW2 Counter',gw2count

	#for j in range(len(arp_table)):
	#  if src1 == arp_table[j][1]:
	#	src_ip = arp_table[j][0]
	#	print 'src_ip',src_ip
		############Compared Counter#################
	#### add flow
	for g in range(len(natnode)):
	   if natnode[g][0][6] == '1':
		print '######channel select############################'
		ch2ip = natnode[g][0][:6]+'3'+natnode[g][0][7:]
		for b in range(len(natnode)):
			if natnode[b][0] == ch2ip:
				break
		if g < b :
			with open(natnode[g][0]+'flow.txt') as file:
				array0 = [[str(digit) for digit in line.split()] for line in file]
			for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
			datapath = self.switches.get(int(src1))
			match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str('10.10.2.0/24'))
			init = parser.OFPFlowMod(datapath, 0, 0, 2, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0, match, instructions)
			datapath.send_msg(init)
			for a in range(1,len(array0),2):
				table_id = 2
				priority = 12300
				match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(array0[a][0]))
				actions = [ofp_parser.OFPActionOutput(3)]
				inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				datapath.send_msg(req)
		if g > b :
			with open(natnode[g][0]+'flow.txt') as file:
				array0 = [[str(digit) for digit in line.split()] for line in file]
			for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
			datapath = self.switches.get(int(src1))
			match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str('10.10.2.0/24'))
			init = parser.OFPFlowMod(datapath, 0, 0, 2, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0, match, instructions)
			datapath.send_msg(init)
			for a in range(0,len(array0),2):
				table_id = 2
				priority = 12300
				match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(array0[a][0]))
				actions = [ofp_parser.OFPActionOutput(3)]
				inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				datapath.send_msg(req)
		print '##############channel select end#####################################'
		global Agw1,newJgw1
		self.ryu_path(gw1[0])
		newJgw1 = newJ
		print 'newjgw1' ,newJgw1
		Agw1 = A
		print 'AGW1' ,Agw1
		global Agw2,newJgw2
		self.ryu_path(gw2[0])
		newJgw2 = newJ
		print 'newJgw2' ,newJgw2
		Agw2 = A
		print 'AGW2' ,Agw2
		if natnode[g][0] not in newJgw1:
			newJgw1[natnode[g][0]] = 1000000
		if natnode[g][0] not in newJgw2:
			newJgw2[natnode[g][0]] = 2000000
	  	#print (newJgw1.get(natnode[g][0]))
		#print (newJgw2.get(natnode[g][0]))	   
		if newJgw1[natnode[g][0]] < newJgw2[natnode[g][0]]:
				print 'gw1 better gw2'
				for i in range(len(Agw1)):
					ryu_path = eval(Agw1[i])
					if ryu_path[0] == natnode[g][0]:
						print 'ryu_path' ,ryu_path
						break
			  	#print ryu_path
				#print src_ip,'input IP addr'
				if len(ryu_path) == 1 and natnode[g][0] != gw1[0] and gw2[0]:
				  print '1 hop to gw1'
				  print 'start IP is' ,natnode[g][0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  #OF msg del.table 30,100
				  if ryu_path[0] == natnode[g][0]:
					#print 'start IP is',ryu_path[0]
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1[0]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)

					ryu_dst = gw1[0]
					print 'to gw1 mac is' ,gw1[1]
					table_id = 30
					priority = 12300
					match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					actions = [ofp_parser.OFPActionSetField(eth_dst=gw1[1]),ofp_parser.OFPActionOutput(1)]
					inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					datapath.send_msg(req)

					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1[0]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print 'map' ,natnode[g][0] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					print "A_1 Hop to gw1",ryu_path[0]
					self.onehopchange(ryu_path[0],gw1[0])
					
				elif len(ryu_path) > 1 and natnode[g][0] != gw1[0] and gw2[0]:
				  print len(ryu_path) ,'hop to gw1'
				  
				  #OF msg del.table 30,100
				  #print '!!',ryu_path[0]
				  for i in range(len(ryu_path)-1):
					print 'start IP is',ryu_path[i] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == ryu_path[i]:
							src1 = ''
							src1 = dp_to_mac[a][1]	
					print ryu_path[i] ,'dpid is',src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i-1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)
					#table_id = 30
					#priority = 12300
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					 #  if dp_to_mac[a][0] == ryu_path[i+1]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[i+1] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[abs(i-1)]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					 #  if dp_to_mac[a][0] == ryu_path[abs(i-1)]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[abs(i-1)] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print ryu_path[i] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

				  print 'repair IP is',ryu_path[0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[0]:
						src1 = ''
						src1 = dp_to_mac[a][1]	
				  print ryu_path[0] ,'dpid is',src1
				  datapath = self.switches.get(int(src1))
				  init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init)
				  init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init1)
				  table_id = 30
				  priority = 12300
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[1]:
						dst_mac = ''
						dst_mac = dp_to_mac[a][2]
				  print 'to' ,ryu_path[1] ,'mac is',dst_mac
				  match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))		
				  actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[1]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_path[0] ,'repair flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'


				  ryu_dst = ryu_path[len(ryu_path)-1]
				  print ryu_dst ,'add flow start to gw!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_dst:
						src1 = ''
						src1 = dp_to_mac[a][1]
						print ryu_dst ,'dpid is' ,src1
				  datapath = self.switches.get(int(src1))
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				 # datapath.send_msg(init)
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw1[0]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  #datapath.send_msg(init)
				  #match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1[0]))
				  #init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
				  #datapath.send_msg(init1)
				  
				 # print 'to gw1 mac is' ,gw1[1]
				  #table_id = 30
				  #priority = 12300
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #actions = [ofp_parser.OFPActionSetField(eth_dst=gw1[1]),ofp_parser.OFPActionOutput(1)]
				 # inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  #req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				 # datapath.send_msg(req)
				 # for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				 #    if dp_to_mac[a][0] == ryu_path[len(ryu_path)-2]:
				#		dst_mac = ''
				#		dst_mac = dp_to_mac[a][2]
				  #print 'to' ,ryu_path[len(ryu_path)-2] ,'mac is',dst_mac
				 # match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw1[0]))
				 # actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				 # inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				 # req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				 # datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1[0]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_dst ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  print len(ryu_path) ,"Hop to gw1" ,ryu_path
				  self.multihopchange(ryu_path,gw1[0])

				else :
				  print 'not start map'
				  return
		  
		elif newJgw2[natnode[g][0]] < newJgw1[natnode[g][0]]:
				print 'gw2 better gw1'
				for i in range(len(Agw2)):
					ryu_path = eval(Agw2[i])
					if ryu_path[0] == natnode[g][0]:
						print 'ryu_path' ,ryu_path
						break
			  	#print ryu_path
				#print src_ip,'input IP addr'
				if len(ryu_path) == 1 and natnode[g][0] != gw1[0] and gw2[0]:
				  print '1 hop to gw2'
				  print 'start IP is' ,natnode[g][0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  #OF msg del.table 30,100
				  if ryu_path[0] == natnode[g][0]:
					#print 'start IP is',ryu_path[0]
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2[0]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)

					ryu_dst = gw2[0]
					print 'to gw2 mac is' ,gw2[1]
					table_id = 30
					priority = 12300
					match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					actions = [ofp_parser.OFPActionSetField(eth_dst=gw2[1]),ofp_parser.OFPActionOutput(1)]
					inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					datapath.send_msg(req)

					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2[0]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print 'map' ,natnode[g][0] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					print "A_1 Hop to gw2",ryu_path[0]
					self.onehopchange(ryu_path[0],gw2[0])
					
				elif len(ryu_path) > 1 and natnode[g][0] != gw1[0] and gw2[0]:
				  print len(ryu_path) ,'hop to gw2'
				  
				  #OF msg del.table 30,100
				  #print '!!',ryu_path[0]
				  for i in range(len(ryu_path)-1):
					print 'start IP is',ryu_path[i] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == ryu_path[i]:
							src1 = ''
							src1 = dp_to_mac[a][1]	
					print ryu_path[i] ,'dpid is',src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i-1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)
					#table_id = 30
					#priority = 12300
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					  # if dp_to_mac[a][0] == ryu_path[i+1]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[i+1] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[abs(i-1)]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					#   if dp_to_mac[a][0] == ryu_path[abs(i-1)]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[abs(i-1)] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print ryu_path[i] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

				  print 'repair IP is',ryu_path[0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[0]:
						src1 = ''
						src1 = dp_to_mac[a][1]	
				  print ryu_path[0] ,'dpid is',src1
				  datapath = self.switches.get(int(src1))
				  init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init)
				  init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init1)
				  table_id = 30
				  priority = 12300
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[1]:
						dst_mac = ''
						dst_mac = dp_to_mac[a][2]
				  print 'to' ,ryu_path[1] ,'mac is',dst_mac
				  match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))		
				  actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[1]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_path[0] ,'repair flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'


				  ryu_dst = ryu_path[len(ryu_path)-1]
				  print ryu_dst ,'add flow start to gw!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_dst:
						src1 = ''
						src1 = dp_to_mac[a][1]
						print ryu_dst ,'dpid is' ,src1
				  datapath = self.switches.get(int(src1))
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				 # datapath.send_msg(init)
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw2[0]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  #datapath.send_msg(init)
				  #match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2[0]))
				  #init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
				  #datapath.send_msg(init1)
				  
				 # print 'to gw2 mac is' ,gw2[1]
				  #table_id = 30
				 # priority = 12300
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				 # actions = [ofp_parser.OFPActionSetField(eth_dst=gw2[1]),ofp_parser.OFPActionOutput(1)]
				  #inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  #req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  #datapath.send_msg(req)
				 # for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				   #  if dp_to_mac[a][0] == ryu_path[len(ryu_path)-2]:
				#		dst_mac = ''
				#		dst_mac = dp_to_mac[a][2]
				  #print 'to' ,ryu_path[len(ryu_path)-2] ,'mac is',dst_mac
				#  match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw2[0]))
				  #actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  #inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  #req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  #datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2[0]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_dst ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  print len(ryu_path) ,"Hop to gw2" ,ryu_path
				  self.multihopchange(ryu_path,gw2[0])

				else :
				  print 'not start map'
				  return
		else :
			print 'not node'
			return
	

	   elif natnode[g][0][6] == '3':
		global Agw1ch2,newJgw1ch2
		self.ryu_path2(gw1ch2[0])
		newJgw1ch2 = newJ
		print 'newjgw1' ,newJgw1ch2
		Agw1ch2 = A
		print 'AGW1' ,Agw1
		global Agw2,newJgw2ch2
		self.ryu_path2(gw2ch2[0])
		newJgw2ch2 = newJ
		print 'newJgw2' ,newJgw2ch2
		Agw2ch2 = A
		print 'AGW2' ,Agw2ch2
		if natnode[g][0] not in newJgw1ch2:
			newJgw1ch2[natnode[g][0]] = 1000000
		if natnode[g][0] not in newJgw2ch2:
			newJgw2ch2[natnode[g][0]] = 2000000
	  	#print (newJgw1.get(natnode[g][0]))
		#print (newJgw2.get(natnode[g][0]))	   
		if newJgw1ch2[natnode[g][0]] < newJgw2ch2[natnode[g][0]]:
				print 'gw1ch2 better gw2ch2'
				for i in range(len(Agw1ch2)):
					ryu_path = eval(Agw1ch2[i])
					if ryu_path[0] == natnode[g][0]:
						print 'ryu_path' ,ryu_path
						break
			  	#print ryu_path
				#print src_ip,'input IP addr'
				if len(ryu_path) == 1 and natnode[g][0] != gw1ch2[0] and gw2ch2[0]:
				  print '1 hop to gw1ch2'
				  print 'start IP is' ,natnode[g][0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  #OF msg del.table 30,100
				  if ryu_path[0] == natnode[g][0]:
					#print 'start IP is',ryu_path[0]
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1ch2[0]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)

					ryu_dst = gw1ch2[0]
					print 'to gw1ch2 mac is' ,gw1ch2[1]
					table_id = 30
					priority = 12300
					match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					actions = [ofp_parser.OFPActionSetField(eth_dst=gw1ch2[1]),ofp_parser.OFPActionOutput(1)]
					inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					datapath.send_msg(req)

					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1ch2[0]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print 'map' ,natnode[g][0] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					print "A_1 Hop to gw1ch2",ryu_path[0]
					self.onehopchange2(ryu_path[0],gw1ch2[0])
					
				elif len(ryu_path) > 1 and natnode[g][0] != gw1ch2[0] and gw2ch2[0]:
				  print len(ryu_path) ,'hop to gw1ch2'
				  
				  #OF msg del.table 30,100
				  #print '!!',ryu_path[0]
				  for i in range(len(ryu_path)-1):
					print 'start IP is',ryu_path[i] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == ryu_path[i]:
							src1 = ''
							src1 = dp_to_mac[a][1]	
					print ryu_path[i] ,'dpid is',src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i-1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)
					#table_id = 30
					#priority = 12300
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					#   if dp_to_mac[a][0] == ryu_path[i+1]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[i+1] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[abs(i-1)]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					#   if dp_to_mac[a][0] == ryu_path[abs(i-1)]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[abs(i-1)] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print ryu_path[i] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

				  print 'repair IP is',ryu_path[0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[0]:
						src1 = ''
						src1 = dp_to_mac[a][1]	
				  print ryu_path[0] ,'dpid is',src1
				  datapath = self.switches.get(int(src1))
				  init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init)
				  init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init1)
				  table_id = 30
				  priority = 12300
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[1]:
						dst_mac = ''
						dst_mac = dp_to_mac[a][2]
				  print 'to' ,ryu_path[1] ,'mac is',dst_mac
				  match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))		
				  actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[1]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_path[0] ,'repair flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'


				  ryu_dst = ryu_path[len(ryu_path)-1]
				  print ryu_dst ,'add flow start to gwch2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_dst:
						src1 = ''
						src1 = dp_to_mac[a][1]
						print ryu_dst ,'dpid is' ,src1
				  datapath = self.switches.get(int(src1))
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				 # datapath.send_msg(init)
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw1ch2[0]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  #datapath.send_msg(init)
				  #match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1ch2[0]))
				  #init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
				  #datapath.send_msg(init1)
				  
				  #print 'to gw1ch2 mac is' ,gw1ch2[1]
				  #table_id = 30
				  #priority = 12300
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #actions = [ofp_parser.OFPActionSetField(eth_dst=gw1ch2[1]),ofp_parser.OFPActionOutput(1)]
				  #inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  #req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  #datapath.send_msg(req)
				  #for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				   #  if dp_to_mac[a][0] == ryu_path[len(ryu_path)-2]:
				#		dst_mac = ''
				#		dst_mac = dp_to_mac[a][2]
				  #print 'to' ,ryu_path[len(ryu_path)-2] ,'mac is',dst_mac
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw1ch2[0]))
				  #actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  #inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				 # req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  #datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw1ch2[0]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_dst ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  print len(ryu_path) ,"Hop to gw1ch2" ,ryu_path
				  self.multihopchange2(ryu_path,gw1ch2[0])

				else :
				  print 'not start map'
				  return
		  
		elif newJgw2ch2[natnode[g][0]] < newJgw1ch2[natnode[g][0]]:
				print 'gw2ch2 better gw1ch2'
				for i in range(len(Agw2ch2)):
					ryu_path = eval(Agw2ch2[i])
					if ryu_path[0] == natnode[g][0]:
						print 'ryu_path' ,ryu_path
						break
			  	#print ryu_path
				#print src_ip,'input IP addr'
				if len(ryu_path) == 1 and natnode[g][0] != gw1ch2[0] and gw2ch2[0]:
				  print '1 hop to gw2ch2'
				  print 'start IP is' ,natnode[g][0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  #OF msg del.table 30,100
				  if ryu_path[0] == natnode[g][0]:
					#print 'start IP is',ryu_path[0]
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == natnode[g][0]:
							src1 = ''
							src1 = dp_to_mac[a][1]
							print natnode[g][0] ,'dpid is' ,src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2ch2[0]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)

					ryu_dst = gw2ch2[0]
					print 'to gw2ch2 mac is' ,gw2ch2[1]
					table_id = 30
					priority = 12300
					match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))
					actions = [ofp_parser.OFPActionSetField(eth_dst=gw2ch2[1]),ofp_parser.OFPActionOutput(1)]
					inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					datapath.send_msg(req)

					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2ch2[0]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print 'map' ,natnode[g][0] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					print "A_1 Hop to gw2ch2",ryu_path[0]
					self.onehopchange2(ryu_path[0],gw2ch2[0])
					
				elif len(ryu_path) > 1 and natnode[g][0] != gw1ch2[0] and gw2ch2[0]:
				  print len(ryu_path) ,'hop to gw2ch2'
				  
				  #OF msg del.table 30,100
				  #print '!!',ryu_path[0]
				  for i in range(len(ryu_path)-1):
					print 'start IP is',ryu_path[i] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
					for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					   if dp_to_mac[a][0] == ryu_path[i]:
							src1 = ''
							src1 = dp_to_mac[a][1]	
					print ryu_path[i] ,'dpid is',src1
					datapath = self.switches.get(int(src1))
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i-1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))
					#init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match, instructions)
					#datapath.send_msg(init)
					#match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))
					#init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
					#datapath.send_msg(init1)
					#table_id = 30
					#priority = 12300
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					#   if dp_to_mac[a][0] == ryu_path[i+1]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[i+1] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[abs(i-1)]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					#for a in range(len(dp_to_mac)):
					   #print dp_to_mac[a][0],dpid
					 #  if dp_to_mac[a][0] == ryu_path[abs(i-1)]:
					#		dst_mac = ''
					#		dst_mac = dp_to_mac[a][2]
					#print 'to' ,ryu_path[abs(i-1)] ,'mac is',dst_mac
					#match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[i+1]))		
					#actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
					#inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
					#req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
					#datapath.send_msg(req)
					table_id1 = 100
					priority = 12300
					match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[i+1]))                
					actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
					inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
					req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
					datapath.send_msg(req1)
					print ryu_path[i] ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'

				  print 'repair IP is',ryu_path[0] ,'add flow start!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[0]:
						src1 = ''
						src1 = dp_to_mac[a][1]	
				  print ryu_path[0] ,'dpid is',src1
				  datapath = self.switches.get(int(src1))
				  init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init)
				  init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  datapath.send_msg(init1)
				  table_id = 30
				  priority = 12300
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_path[1]:
						dst_mac = ''
						dst_mac = dp_to_mac[a][2]
				  print 'to' ,ryu_path[1] ,'mac is',dst_mac
				  match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[0]))		
				  actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				  inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				  req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(ryu_path[1]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_path[0] ,'repair flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'


				  ryu_dst = ryu_path[len(ryu_path)-1]
				  print ryu_dst ,'add flow start to gwch2!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				     if dp_to_mac[a][0] == ryu_dst:
						src1 = ''
						src1 = dp_to_mac[a][1]
						print ryu_dst ,'dpid is' ,src1
				  datapath = self.switches.get(int(src1))
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				 # datapath.send_msg(init)
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw2ch2[0]))
				  #init = parser.OFPFlowMod(datapath, 0, 0, 30, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,None, instructions)
				  #datapath.send_msg(init)
				  #match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2ch2[0]))
				  #init1 = parser.OFPFlowMod(datapath, 0, 0, 100, ofp.OFPFC_DELETE, 0, 0, 12300,ofp.OFPCML_NO_BUFFER, ofp.OFPP_ANY, ofp.OFPG_ANY, 0,match1, instructions)
				  #datapath.send_msg(init1)
				  
				  #print 'to gw2ch2 mac is' ,gw2ch2[1]
				 # table_id = 30
				 # priority = 12300
				  #match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_path[len(ryu_path)-2]))
				 # actions = [ofp_parser.OFPActionSetField(eth_dst=gw2ch2[1]),ofp_parser.OFPActionOutput(1)]
				  #inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				 # req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				  #datapath.send_msg(req)
				  #for a in range(len(dp_to_mac)):
				     #print dp_to_mac[a][0],dpid
				  #   if dp_to_mac[a][0] == ryu_path[len(ryu_path)-2]:
				#		dst_mac = ''
				#		dst_mac = dp_to_mac[a][2]
				  #print 'to' ,ryu_path[len(ryu_path)-2] ,'mac is',dst_mac
				 # match =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(gw2ch2[0]))
				 # actions = [ofp_parser.OFPActionSetField(eth_dst=dst_mac),ofp_parser.OFPActionOutput(1)]
				 # inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions)]
				 # req = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match, inst)
				 # datapath.send_msg(req)
				  table_id1 = 100
				  priority = 12300
				  match1 =ofp_parser.OFPMatch(eth_type=0x0800,ipv4_dst=str(gw2ch2[0]))                
				  actions1 = [ofp_parser.OFPActionOutput(ofp.OFPP_NORMAL, 0)]
				  inst1 = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS,actions1)]
				  req1 = ofp_parser.OFPFlowMod(datapath, cookie, cookie_mask,table_id1, ofp.OFPFC_ADD,idle_timeout, hard_timeout,priority, buffer_id,ofp.OFPP_ANY, ofp.OFPG_ANY,ofp.OFPFF_SEND_FLOW_REM,match1, inst1)
				  datapath.send_msg(req1)
				  print ryu_dst ,'add flow end!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
				  print len(ryu_path) ,"Hop to gw2ch2" ,ryu_path
				  self.multihopchange2(ryu_path,gw2ch2[0])

				else :
				  print 'not start map'
				  return
		else :
			print 'not node'
			return

        ##Focus on NAT node##

        #match = ofp_parser.OFPMatch(eth_type=0x0800,ipv4_src=str(ryu_dst))
        #actions = [ofp_parser.OFPActionSetField(eth_dst="00:00:00:00:00:aa"),ofp_parser.OFPActionOutput(1)]

          
	if eth.ethertype == ether_types.ETH_TYPE_LLDP:
		# ignore lldp packet
		return


	self.mac_to_port.setdefault(dpid, {})

	self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)
	
	# learn a mac address to avoid FLOOD next time.
	self.mac_to_port[dpid][src] = in_port
	 
	if dst in self.mac_to_port[dpid]:
		out_port = self.mac_to_port[dpid][dst]

	else:
		out_port = ofproto.OFPP_FLOOD

	actions = [parser.OFPActionOutput(out_port)]
 

	# install a flow to avoid packet_in next time
	if out_port != ofproto.OFPP_FLOOD:
		match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
		# verify if we have a valid buffer_id, if yes avoid to send both
		# flow_mod & packet_out
		if msg.buffer_id != ofproto.OFP_NO_BUFFER:
			self.add_flow(datapath, 1, match, actions, msg.buffer_id)
			return
		else:
			self.add_flow(datapath, 1, match, actions)
	data = None
	if msg.buffer_id == ofproto.OFP_NO_BUFFER:
		data = msg.data

	out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
							  in_port=in_port, actions=actions, data=data)

	datapath.send_msg(out)
        



